# Game of Life
This project is a terminal version of Conway's "Game of Life" implemented in C. It is a zero-player game where there's a grid of dead and live cells. These cells follow the next rules:
 - A dead cell with exactly 3 live neighbors(cells) borns.
 - A live cell with 2 or 3 live neighbors keeps alive, in other case it dies by loneliness or overpopulation.

Source: https://es.wikipedia.org/wiki/Juego_de_la_vida

# 