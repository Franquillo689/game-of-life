#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

// Num of columns
#define COLUMNS 20
// Num of rows
#define ROWS 20

bool board[ROWS][COLUMNS];
bool nextBoard[ROWS][COLUMNS];
// Time between updates of the board (in micro-seconds)
long int delta = 500*1000;

void update(int y, int x){
	// Coordinates of the neighbors
	int neighbors[8][2] = {
			{y-1,x-1},
			{y-1, x },
			{y-1,x+1},
			{ y ,x-1},
			{ y ,x+1},
			{y+1,x-1},
			{y+1, x },
			{y+1,x+1} };
	// Amount of neighbors of cell in board[y][x]
	int num = 0;
	/* 
	 * The next for loop makes sure that
	 * the coordinates of each neighbors
	 * exist. 
	 */
	for(int i = 0;i < 8;i++){
		neighbors[i][0] = neighbors[i][0] % COLUMNS;
		neighbors[i][1] = neighbors[i][1] % ROWS;
		num += (int)board[neighbors[i][0]][neighbors[i][1]];
	}
	// It returns if the cell is going to live in next generation or not
	nextBoard[y][x] = (bool) ~(((num>>3)&1)|((num>>2)&1))&((num&1)|board[y][x])&((num>>1)&1);
}

/* structures in game of life */
void createGlider(unsigned int y, unsigned int x){
	if( y > ROWS || x > COLUMNS ) return;
	board[y][x] = 1;
	board[y+1][x+1] = 1;
	board[y+2][x-1] = 1;
	board[y+2][x] = 1;
	board[y+2][x+1] = 1;
}

void createSquare(unsigned int y, unsigned int x){
	if( y > ROWS || x > COLUMNS ) return;
	board[y][x] = 1;
	board[y][x+1] = 1;
	board[y+1][x] = 1;
	board[y+1][x+1] = 1;
}

void createRpentomino(unsigned int y, unsigned int x){
	if( y > ROWS || x > COLUMNS ) return;
	board[y][x] = 1;
	board[y-1][x] = 1;
	board[y-1][x+1] = 1;
	board[y][x-1] = 1;
	board[y+1][x] = 1;
}

int main(){
	createRpentomino((int)ROWS/2,(int)COLUMNS/2);
	system("clear");
	int gen = 0;
	for(;;){
		printf("\x1b[H");
		for(int i = 0;i < ROWS;i++){
			for(int j = 0;j < COLUMNS;j++){
				update(i, j);
			}
		}
		usleep(delta);
		for(int i = 0;i < ROWS;i++){
			for(int j = 0;j < COLUMNS;j++){
				printf( board[i][j] ? "@":" ");
				board[i][j] = nextBoard[i][j];
			}
			printf("\n");
		}
		printf("Generation: %i",gen);
		gen += 1;
	}
	return 0;
}
